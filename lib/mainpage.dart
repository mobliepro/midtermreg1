import 'package:first_page/login.dart';
import 'package:first_page/profilepage.dart';
import 'package:flutter/material.dart';

String currentPage = "LoginPage";

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.blue,
      ),
    );
  }
}

class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'Burapha University',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 30,),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
            onTap: () {
              if(currentPage == "MainPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => ProfileScreen(),
                ));
                currentPage = "MainPage";
              }
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () {
              if(currentPage == "MainPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => Login(),
                ));
                currentPage = "MainPage";
              }
            },
          ),
        ],
      ),
    );
  }
}
