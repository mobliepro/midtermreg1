import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreen createState() => _ProfileScreen();
}

class _ProfileScreen extends State<ProfileScreen> {
  final double coverHeing = 280;
  final double profileHeingt = 144;
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body:ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          buildTop(),
          buildContent(),
        ]
        
      )
      
    );
  }
  Widget buildTop() {
    final bottom = profileHeingt /2;
    final top = coverHeing - profileHeingt /2;
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center ,
        children:[
          Container(
            margin: EdgeInsets.only(bottom: bottom),
            child: buildCoverImage(),
          ),
          
          Positioned(
            top: top,
            child:buildProfileImage(), 
            ),
          
        ]
    );
}
  Widget buildCoverImage() => Container(
    color: Colors.white,
    child: Image.network('https://images.unsplash.com/photo-1637074903306-f1a621cd6307?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80'),
    width: double.infinity,
    height: coverHeing,
     
  ) ;
   Widget buildProfileImage() => CircleAvatar(
    radius: profileHeingt / 2 ,
    backgroundColor: Colors.white,
    backgroundImage: NetworkImage(
      'https://i.pinimg.com/564x/e5/11/5e/e5115edac2de1f68400c21f625722576.jpg'),
  );
  
  Widget buildContent() => Column(
    children: [
      const SizedBox(height: 8),
      Text('Sarocha Chanachai',style: TextStyle(fontSize: 28),
      ),
     Padding(
      padding: EdgeInsets.only(
        left: 30,
        right: 30,
        top: 10,
        bottom: 10,
      ),
      child: Divider(
        thickness: 1,
        color: Colors.grey,
       ),
      ), 
      Text('Organization', style: TextStyle(fontWeight:FontWeight.bold, fontSize: 22),),
      Text('ID: 63160014', style: TextStyle(fontSize: 18),),
      Text('User Type: Student', style: TextStyle(fontSize: 18),),
      Text('Faculty: Informatics',style: TextStyle(fontSize: 18),),
      Text('Year: 3', style: TextStyle(fontSize: 18),),
      Text('Email: 63160014@go.buu.ac.th', style: TextStyle(fontSize: 18),)
    ],
  );
}